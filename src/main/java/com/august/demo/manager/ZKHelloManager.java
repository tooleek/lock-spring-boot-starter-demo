package com.august.demo.manager;

import org.springframework.stereotype.Component;

import io.gitee.tooleek.lock.spring.boot.annotation.Key;
import io.gitee.tooleek.lock.spring.boot.annotation.Lock;
import io.gitee.tooleek.lock.spring.boot.annotation.RLock;
import io.gitee.tooleek.lock.spring.boot.annotation.ZLock;
import io.gitee.tooleek.lock.spring.boot.enumeration.InterProcess;

@Component
public class ZKHelloManager {

	@ZLock
	public void sayHelloForZ(@Key Long id) {
		System.out.println("hello world "+Thread.currentThread().getName());
	}
	
	@Lock
	public void sayHello(@Key Long id) {
		System.out.println("hello world "+Thread.currentThread().getName());
	}
	
	@RLock
	public void sayHelloForR(@Key Long id) {
		System.out.println("hello world "+Thread.currentThread().getName());
	}
	
}
