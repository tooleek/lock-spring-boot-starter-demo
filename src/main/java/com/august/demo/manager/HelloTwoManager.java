package com.august.demo.manager;

import org.springframework.stereotype.Component;

import io.gitee.tooleek.lock.spring.boot.annotation.Key;
import io.gitee.tooleek.lock.spring.boot.annotation.Lock;
import io.gitee.tooleek.lock.spring.boot.enumeration.LockType;

@Component
public class HelloTwoManager {

	@Lock(lockType=LockType.FAIR,leaseTime=50)
	public void twoReentrant(String ces, @Key String orderNo) {
		System.out.println("hello  two  "+Thread.currentThread().getName());
	}
	
}
